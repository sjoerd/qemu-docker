FROM docker.io/kernelci/qemu

RUN mkdir -p /etc/qemu
RUN echo allow all > /etc/qemu/bridge.conf
